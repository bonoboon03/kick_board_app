import 'package:flutter/material.dart';

class ComponentUseHistoryDetail extends StatelessWidget {
  const ComponentUseHistoryDetail({super.key, required this.text1, required this.text2});

  final String text1;
  final String text2;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(text1,
          style: const TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 20,
          ),
        ),
        Text(text2,
          style: const TextStyle(
            fontSize: 20,
          ),
        ),
      ],
    );
  }
}

