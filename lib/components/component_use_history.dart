import 'package:flutter/material.dart';
import 'package:kick_board_app/pages/page_use_history_detail.dart';

class ComponentUseHistory extends StatelessWidget {
  const ComponentUseHistory({super.key, required this.dateStart, required this.useTime});

  final String dateStart;
  final String useTime;


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageUseHistoryDetail()));
      },
      child: Container(
        padding: const EdgeInsets.fromLTRB(20, 25, 10, 25),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          border: Border.all(
            width: 1,
            color: const Color.fromRGBO(204, 204, 204, 100),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(dateStart,
                  style: const TextStyle(
                    fontSize: 22,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(useTime,
                  style: const TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 24,
                  ),
                ),
              ],
            ),
            const Icon(Icons.keyboard_arrow_right,
              size: 35,
            ),
          ],
        ),
      ),
    );
  }
}
