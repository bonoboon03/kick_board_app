class UseHistoryItem {
  int id;
  String dateStart;
  String useTime;
  
  UseHistoryItem(this.id, this.dateStart, this.useTime);
  
  factory UseHistoryItem.fromJson(Map<String, dynamic> json) {
    return UseHistoryItem(
      json['id'],
      json['dateStart'],
      json['useTime'],
    );
  }
}