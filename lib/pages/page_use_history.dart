import 'package:flutter/material.dart';
import 'package:kick_board_app/components/component_use_history.dart';

class PageUseHistory extends StatefulWidget {
  const PageUseHistory({super.key});

  @override
  State<PageUseHistory> createState() => _PageUseHistoryState();
}

class _PageUseHistoryState extends State<PageUseHistory> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("이용기록"),
        backgroundColor: const Color.fromRGBO(243, 150, 150, 100),
      ),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            ComponentUseHistory(dateStart: "", useTime: "",),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}
