import 'package:flutter/material.dart';
import 'package:kick_board_app/components/component_use_history_detail.dart';

class PageUseHistoryDetail extends StatefulWidget {
  const PageUseHistoryDetail({super.key});

  @override
  State<PageUseHistoryDetail> createState() => _PageUseHistoryDetailState();
}

class _PageUseHistoryDetailState extends State<PageUseHistoryDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("이용내역"),
        backgroundColor: const Color.fromRGBO(243, 150, 150, 100),
      ),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Row(
              children: [
                CircleAvatar(radius: 30,
                  backgroundColor: Color.fromRGBO(243, 150, 150, 100),
                ),
                SizedBox(
                  width: 20,
                ),
                Center(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text("최애",
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 24,
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text("님",
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 20,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 25,
            ),
            const Text("이용 내역",
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 22,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              padding: const EdgeInsets.all(15),
              decoration: BoxDecoration(
                border: Border.all(
                  width: 1,
                  color: const Color.fromRGBO(204, 204, 204, 100),
                ),
                borderRadius: BorderRadius.circular(4),
              ),
              child: const Column(
                children: [
                  ComponentUseHistoryDetail(text1: "최애 번호", text2: "123456"),
                  SizedBox(
                    height: 15,
                  ),
                  ComponentUseHistoryDetail(text1: "탑승 일자", text2: "2023-07-27(목)"),
                  SizedBox(
                    height: 15,
                  ),
                  ComponentUseHistoryDetail(text1: "탑승 시작 시간", text2: "오전 9:30"),
                  SizedBox(
                    height: 15,
                  ),
                  ComponentUseHistoryDetail(text1: "탑승 종료 시간", text2: "오전 9:50"),
                  SizedBox(
                    height: 15,
                  ),
                  ComponentUseHistoryDetail(text1: "최애포인트", text2: "500 포인트"),
                ],
              ),
            ),
            const SizedBox(
              height: 25,
            ),
            const Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("최종 결제 요금",
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 22,
                  ),
                ),
                Text("2,400원",
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 22,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              padding: const EdgeInsets.all(15),
              decoration: BoxDecoration(
                border: Border.all(
                  width: 1,
                  color: const Color.fromRGBO(204, 204, 204, 100),
                ),
                borderRadius: BorderRadius.circular(4),
              ),
              child: const Column(
                children: [
                  ComponentUseHistoryDetail(text1: "이용 요금", text2: "2,400원"),
                  SizedBox(
                    height:10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("최애 플랜",
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 18,
                          color: Colors.grey,
                        ),
                      ),
                      Text("1,200원",
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.grey,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("라이딩 요금(분당 요금)",
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 18,
                          color: Colors.grey,
                        ),
                      ),
                      SizedBox(
                        child: Row(
                          children: [
                            Text("1,200원",
                              style: TextStyle(
                                fontSize: 18,
                                color: Colors.grey,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
