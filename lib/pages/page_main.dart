import 'package:flutter/material.dart';
import 'package:kick_board_app/pages/page_coupon.dart';
import 'package:kick_board_app/pages/page_customer_service.dart';
import 'package:kick_board_app/pages/page_my_info.dart';
import 'package:kick_board_app/pages/page_plan.dart';
import 'package:kick_board_app/pages/page_use_history.dart';

class PageMain extends StatefulWidget {
  const PageMain({super.key});

  @override
  State<PageMain> createState() => _PageMainState();
}

class _PageMainState extends State<PageMain> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("최애의 킥보드"),
        backgroundColor: const Color.fromRGBO(243, 150, 150, 100),
      ),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {},
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.42,
                      height: MediaQuery.of(context).size.width * 0.42,
                      decoration: BoxDecoration(
                        border: Border.all(
                          width: 1,
                          color: const Color.fromRGBO(204, 204, 204, 100),
                        ),
                        borderRadius: BorderRadius.circular(4),
                      ),
                      child: const Center(
                        child: Icon(Icons.map,
                          size: 30,
                        ),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {},
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.42,
                      height: MediaQuery.of(context).size.width * 0.42,
                      decoration: BoxDecoration(
                        border: Border.all(
                          width: 1,
                          color: const Color.fromRGBO(204, 204, 204, 100),
                        ),
                        borderRadius: BorderRadius.circular(4),
                      ),
                      child: const Center(
                        child: Text("시작하기",
                          style: TextStyle(
                            fontSize: 24,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageMyInfo()));
                },
                child: Container(
                  padding: const EdgeInsets.all(20),
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 1,
                      color: const Color.fromRGBO(204, 204, 204, 100),
                    ),
                    borderRadius: BorderRadius.circular(4),
                  ),
                  child: const Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        children: [
                          CircleAvatar(radius: 30,
                            backgroundColor: Color.fromRGBO(243, 150, 150, 100),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text("최애",
                            style: TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Column(
                            children: [
                              Row(
                                children: [
                                  Text("보유 포인트",
                                    style: TextStyle(
                                      fontSize: 24,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text("500",
                                    style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      fontSize: 28,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * 0.42,
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 1,
                        color: const Color.fromRGBO(204, 204, 204, 100),
                      ),
                      borderRadius: BorderRadius.circular(4),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Icon(Icons.card_giftcard),
                        MaterialButton(
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageCoupon()));
                          },
                          child: const Text("쿠폰")
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.42,
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 1,
                        color: const Color.fromRGBO(204, 204, 204, 100),
                      ),
                      borderRadius: BorderRadius.circular(4),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Icon(Icons.headset_mic),
                        MaterialButton(
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageCustomerService()));
                          },
                          child: const Text("고객센터"),
                        ),
                      ],
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
      drawer: Drawer(
        child: ListView(
          padding: const EdgeInsets.all(20),
          children: [
            const DrawerHeader(
              child: Row(
                children: [
                  CircleAvatar(radius: 30,
                    backgroundColor: Color.fromRGBO(243, 150, 150, 100),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Text("최애",
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 24,
                    ),
                  ),
                ],
              ),
            ),
            ListTile(
              leading: const Icon(Icons.person),
              title: const Text('나의 정보'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageMyInfo()));
              },
            ),
            ListTile(
              leading: const Icon(Icons.assignment),
              title: const Text('이용내역'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageUseHistory()));
              },
            ),
            ListTile(
              leading: const Icon(Icons.card_giftcard),
              title: const Text('쿠폰함'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageCoupon()));
              },
            ),
            ListTile(
              leading: const Icon(Icons.info_outline),
              title: const Text('플랜소개'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PagePlan()));
              },
            ),
            ListTile(
              leading: const Icon(Icons.headset_mic),
              title: const Text('고객센터'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageCustomerService()));
              },
            ),
          ],
        ),
      ),
    );
  }
}
