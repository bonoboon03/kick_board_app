import 'package:flutter/material.dart';
import 'package:kick_board_app/components/component_plan.dart';

class PagePlan extends StatefulWidget {
  const PagePlan({super.key});

  @override
  State<PagePlan> createState() => _PagePlanState();
}

class _PagePlanState extends State<PagePlan> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("플랜소개"),
        backgroundColor: const Color.fromRGBO(243, 150, 150, 100),
      ),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            const ComponentPlan(title: "삼애 플랜", text1: "대인: 최대 100만원", text2: "대물: 최대 100만원", text3: "자손: 최대 20만원", text4: "기기: 최대 50만원"),
            const SizedBox(
              height: 20,
            ),
            const ComponentPlan(title: "차애 플랜", text1: "대인: 최대 50만원", text2: "대물: 최대 50만원", text3: "자손: 최대 20만원", text4: "기기: 최대 10만원"),
            const SizedBox(
              height: 20,
            ),
            const ComponentPlan(title: "차애 플랜", text1: "대인: 최대 30만원", text2: "대물: 최대 30만원", text3: "자손: 최대 20만원", text4: "기기: 완전 면책"),
            const SizedBox(
              height: 20,
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4.0),
                border: Border.all(
                  width: 1,
                  color: const Color.fromRGBO(204, 204, 204, 100),
                ),
              ),
              padding: const EdgeInsets.all(20),
              child: const Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("분당 추가요금",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 24,
                    ),
                  ),
                  Text("150원",
                    style: TextStyle(
                      fontSize: 24,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
